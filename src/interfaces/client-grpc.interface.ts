import type {Observable} from "rxjs";

type EnsureObservable<T> = T extends Observable<any> ? T : (T extends Promise<infer V> ? Observable<V> : never);

export type GrpcService<T extends Record<string, (...args: any) => any>> = {
    [K in keyof T]: (...args: Parameters<T[K]>) => EnsureObservable<ReturnType<T[K]>>
};

export interface ClientGrpc {
    getService<T extends Record<string, (...args: any) => any>>(name: string): GrpcService<T>;
}
