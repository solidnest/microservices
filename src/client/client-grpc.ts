import {ClientGrpcProxy as FaultyClientGrpcProxy} from "@nestjs/microservices";
import type {ClientGrpc, GrpcService} from "../interfaces/client-grpc.interface";

export class ClientGrpcProxy extends FaultyClientGrpcProxy implements ClientGrpc {
    // @ts-ignore
    getService<T extends {}>(name: string): GrpcService<T> {
        return super.getService(name);
    }
}